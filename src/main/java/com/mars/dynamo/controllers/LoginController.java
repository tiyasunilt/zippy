package com.mars.dynamo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mars.dynamo.entity.UserDetail;
import com.mars.dynamo.models.LoginRequest;
import com.mars.dynamo.models.LoginResponse;
import com.mars.dynamo.repository.UserDetailRepository;

@RestController
@CrossOrigin
@RequestMapping("/login")
public class LoginController {
 @Autowired	
 private UserDetailRepository repository;
 @PostMapping
 public ResponseEntity<?> handleLoginFunctionality(@RequestBody LoginRequest loginData){
	 String email = loginData.getEmail();
	 String password = loginData.getPassword();
	 
	 UserDetail userDetail = repository.findById(email);
	 if(userDetail != null) {
	 String existingPassword = userDetail.getPassword();
	 
	 if(password.equalsIgnoreCase(existingPassword)) {
		 LoginResponse response = new LoginResponse();
		 response.setEmail(email);
		 response.setFirstName(userDetail.getFirstName());
		 response.setLastName(userDetail.getLastName());
		 response.setLoginStatus(userDetail.getLoginState());
		 response.setRoleId(userDetail.getRoleid());
		 
		 userDetail.setLoginAttemps(0);
		 repository.save(userDetail);
		 
		 return new ResponseEntity<LoginResponse>(response, HttpStatus.OK);
	 }
	 else if(!(password.equals(userDetail.getPassword()))) {
		 LoginResponse response = new LoginResponse();
		 response.setFirstName(userDetail.getFirstName());
		 response.setLastName(userDetail.getLastName());
		 
		 userDetail.setLoginAttemps(userDetail.getLoginAttemps()+1);
		 repository.save(userDetail);
		 response.setLoginAttempts(userDetail.getLoginAttemps());
		 
		 if(userDetail.getLoginAttemps() >= 5) {
			 System.out.println("logic to lock account");
		 }
		 
		 response.setLoginAttempts(userDetail.getLoginAttemps());
		 
		 return new ResponseEntity<LoginResponse>(response, HttpStatus.UNAUTHORIZED);		 
	 }
   }//if
   return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
 }
}
