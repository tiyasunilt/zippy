package com.mars.dynamo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mars.dynamo.entity.UserDetail;
import com.mars.dynamo.repository.UserDetailRepository;

@RestController
@RequestMapping("/users")
public class UserDetailsController {
	@Autowired
	private UserDetailRepository repository;
	
	/**
	 * 
	 * @param userDetail
	 * @return ResponseEntity<String>
	 */
	@PostMapping("/new")
	public ResponseEntity<?> addNewUser(@RequestBody UserDetail userDetail) {
		userDetail.setPassword("Abc@12345");
		userDetail.setLoginState(0);
		userDetail.setLoginAttemps(0);
		
		System.out.println(userDetail);
		
		UserDetail user = repository.save(userDetail);		
		
		if(user != null) 
			return new ResponseEntity<String>("User Created Successfully", HttpStatus.CREATED);
		
		return new ResponseEntity<String>("User Creation Failed!!", HttpStatus.BAD_REQUEST);
		
	}
}
