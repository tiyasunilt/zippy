package com.mars.dynamo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mars.dynamo.entity.UserDetail;
import com.mars.dynamo.models.LoginRequest;
import com.mars.dynamo.repository.UserDetailRepository;
import com.mars.dynamo.util.JwtUtil;

@RestController
@CrossOrigin
public class AuthenticationController {

	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private UserDetailRepository repository;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@CrossOrigin
	@PostMapping("/authenticate")
	public ResponseEntity<?> generateToken(@RequestBody LoginRequest loginRequest) throws Exception {
		UserDetail userDetail = repository.findById(loginRequest.getEmail());
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
		} catch (Exception ex) {
			//throw new Exception("invalid username/password");
			if(userDetail != null) {
				userDetail.setLoginAttemps(userDetail.getLoginAttemps()+1);
				repository.save(userDetail);
			}
			return new ResponseEntity<String>("Invalid Username/ Password", HttpStatus.UNAUTHORIZED);
		}
		
		
		return new ResponseEntity<>(jwtUtil.generateToken(userDetail), HttpStatus.OK);
	}
}
