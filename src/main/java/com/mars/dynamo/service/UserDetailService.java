package com.mars.dynamo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mars.dynamo.entity.UserDetail;
import com.mars.dynamo.repository.UserDetailRepository;

@Service
public class UserDetailService implements UserDetailsService {
    @Autowired
    private UserDetailRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDetail userDetail = repository.findById(email);
        System.out.println(userDetail);
        org.springframework.security.core.userdetails.UserDetails user = new org.springframework.security.core.userdetails.User(userDetail.getEmail(), userDetail.getPassword(), new ArrayList<>());
        return user;
    }
}