package com.mars.dynamo.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.mars.dynamo.entity.UserDetail;

@Repository
public class UserDetailRepository {
    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public UserDetail save(UserDetail user){
        dynamoDBMapper.save(user);
        return user;
    }

    public UserDetail findById(String emailid){
       return dynamoDBMapper.load(UserDetail.class, emailid);
    }

    public List<UserDetail> findAll(){
        return dynamoDBMapper.scan(UserDetail.class, new DynamoDBScanExpression());
    }

    public String update(String emailid, UserDetail user){
        dynamoDBMapper.save(user,
                new DynamoDBSaveExpression()
        .withExpectedEntry("email",
                new ExpectedAttributeValue(
                        new AttributeValue().withS(emailid)
                )));
        return emailid;
    }

    public String delete(String id){
       UserDetail user = dynamoDBMapper.load(UserDetail.class, id);
        dynamoDBMapper.delete(user);
        return "User deleted successfully:: "+id;
    }
}
