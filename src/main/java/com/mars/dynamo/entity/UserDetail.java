package com.mars.dynamo.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "zippy-dtls")
public class UserDetail {
	@DynamoDBHashKey
	private String email;
	
	@DynamoDBAttribute
	private Integer roleid;
	
	@DynamoDBAttribute
	private String firstName;
	
	@DynamoDBAttribute
	private String lastName;
	
	@DynamoDBAttribute
	private String phone;
	
	@DynamoDBAttribute
	private String skills;
	
	@DynamoDBAttribute
	private Float yearsOfExperience;
	
	@DynamoDBAttribute
	private String jobTitle;
	
	@DynamoDBAttribute
	private String password;
	
	@DynamoDBAttribute
	private Integer loginState;
	
	@DynamoDBAttribute
	private Integer loginAttemps;
}
