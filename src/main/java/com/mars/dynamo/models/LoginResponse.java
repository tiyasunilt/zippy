package com.mars.dynamo.models;

import lombok.Data;

@Data
public class LoginResponse {
  private String email;
  private String firstName;
  private String lastName;
  private Integer loginAttempts;
  private Integer loginStatus;
  private Integer roleId; 
}
